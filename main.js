function valoresDamage(damages) {
    num = [];
    dados = [];
    for (damage of damages) {
        if (damage.indexOf('d') === 1) {
            n = damage[0];
            if (damage.length === 4) {
                dado = damage[2] + damage[3];
            } else if (damage.length === 3) {
                dado = damage[2];
            }
            num.push(n);
            dados.push(dado);
        } else if (damage.indexOf('d') === 2) {
            n = damage[0] + damage[1];
            if (damage.length === 5) {
                dado = damage[3] + damage[4];
            } else if (damage.length === 4) {
                dado = damage[3];
            }
            num.push(n);
            dados.push(dado);
        }
    }
    return [num, dados];
}

function stop(player) {
    reset();
    div$$ = document.getElementById('gameover');
    div$$.innerHTML = 'Game over: ' + player.name + ' win!';
}

let valorTotal1 = 0;
let valorTotal2 = 0;

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

round = 0;

const fight = (players, div$$) => {
    round++;

    div$$.innerHTML = ' ';
    h2$$ = document.createElement('h2');
    h2$$.innerHTML = 'Round ' + round;
    div$$.appendChild(h2$$);

    damage1 = players[0].damage;
    damage2 = players[1].damage;
    n1 = valoresDamage(damage1)[0];
    dado1 = valoresDamage(damage1)[1];
    n2 = valoresDamage(damage2)[0];
    dado2 = valoresDamage(damage2)[1];

    let valor1 = 0;
    let valor2 = 0;

    vida1 = players[0].vitality - valorTotal2;
    vida2 = players[1].vitality - valorTotal1;

    h31$$ = document.createElement('h3');
    h31$$.innerHTML = 'Player1:'
    div$$.appendChild(h31$$);

    for (i = 0; i < n1.length; i++) { //recorro el array de n1

        for (j = 0; j < n1[i]; j++) { //para la posición x del elemento de n1, repito la matemática n veces el valor del elemento
            valor1 = Math.floor(getRandomArbitrary(1, dado1[i]));

            if (valor1 === players[0].critic) {
                valor1 = valor1 * 2;
            }
            p$$ = document.createElement('p');
            p$$.innerHTML = valor1;
            p$$.innerHTML = '+ ' + valor1;
            h31$$.appendChild(p$$)
            valorTotal1 = valorTotal1 + valor1;
        }
    }
    valorTotal1 = valorTotal1 - players[1].defense;
    vida2 = vida2 - valorTotal1;
    p1$$ = document.createElement('p');
    p1$$.innerHTML = '-' + players[1].defense + '= ' + valorTotal1; // + '<br>Vida: ' + vida1;
    div$$.appendChild(p1$$);

    h32$$ = document.createElement('h3');
    h32$$.innerHTML = 'Player2:'
    div$$.appendChild(h32$$);

    for (i = 0; i < n2.length; i++) { //recorro el array de n1

        for (j = 0; j < n2[i]; j++) { //para la posición x del elemento de n1, repito la matemática n veces el valor del elemento
            valor2 = Math.floor(getRandomArbitrary(1, dado2[i]));

            if (valor2 === players[1].critic) {
                valor2 = valor2 * 2;
            }
            p$$ = document.createElement('p');
            p$$.innerHTML = valor2;
            p$$.innerHTML = '+ ' + valor2;
            h32$$.appendChild(p$$);
            valorTotal2 = valorTotal2 + valor2;
        }
    }
    valorTotal2 = valorTotal2 - players[0].defense;
    vida1 = vida1 - valorTotal2;
    p2$$ = document.createElement('p');
    p2$$.innerHTML = '-' + players[0].defense + '= ' + valorTotal2; //+ '<br>Vida: ' + vida2;

    div$$.appendChild(p2$$);

    h5$$ = document.createElement('h5');
    h5$$.innerHTML = 'Vida ' + players[0].name + '= ' + vida1 + ' Vs. vida ' + players[1].name + '= ' + vida2;
    div$$.appendChild(h5$$);

    if (vida1 <= 0 && vida2 <= 0) {
        setTimeout(function () {
            stop(players[0]);

        }, 1000);
    } else if (vida1 <= 0) {
        setTimeout(function () {
            stop(players[1]);

        }, 1000);
    } else if (vida2 <= 0) {
        setTimeout(function () {
            stop(players[0]);

        }, 1000);
    }
}

const printFigther = async (players) => {
    reset();

    batalla$$ = document.getElementById('battle');
    title$$ = document.getElementById('title');

    idPlayer1 = players[0].id;
    idPlayer2 = players[1].id;

    const player1 = await fetch('http://localhost:3000/characters/' + idPlayer1).then(ids => ids.json());
    const player2 = await fetch('http://localhost:3000/characters/' + idPlayer2).then(ids => ids.json());

    players = [player1, player2];

    h2$$ = document.createElement('h2');
    h2$$.innerHTML = 'Que comience la batalla entre:<br><br>' + player1.name + ' y ' + player2.name + '!';
    title$$.appendChild(h2$$);

    div$$ = document.createElement('div');
    div$$.classList.add('left');

    for (player of players) {
        let article$$ = document.createElement('article');
        let h3$$ = document.createElement('h3');
        h3$$.innerHTML = player.name;
        let img$$ = document.createElement('img');
        img$$.src = player.avatar;
        let h5$$ = document.createElement('h5');
        h5$$.innerHTML = 'Estadísticas';
        let h4$$ = document.createElement('h4');
        vida$$ = player.vitality;
        h4$$.innerHTML = 'Vida: ' + vida$$;
        let ul$$ = document.createElement('ul');
        for (propiedad in player) {
            if (propiedad === 'critic' || propiedad === 'defense') {
                li$$ = document.createElement('li')
                li$$.innerHTML = propiedad + ': ' + player[propiedad];
                ul$$.appendChild(li$$);
            }
        }

        article$$.appendChild(h3$$);
        article$$.appendChild(h4$$)
        article$$.appendChild(img$$);
        article$$.appendChild(h5$$);
        article$$.appendChild(ul$$);

        div$$.appendChild(article$$);
    }
    batalla$$.appendChild(div$$);

    divlucha$$ = document.createElement('div');
    divlucha$$.classList.add('battle');
    divres$$ = document.createElement('div');
    divres$$.classList.add('results');
    btn$$ = document.createElement('button');
    btn$$.innerHTML = 'Lucha!';
    btn$$.classList.add('lucha');
    btn$$.addEventListener('click', function () {
        fight(players, divres$$);
    })
    divlucha$$.appendChild(btn$$)

    divlucha$$.appendChild(divres$$);
    batalla$$.appendChild(divlucha$$);
}

const printCharacters = async () => { //esta función me pinta los 5 personajes iniciales con sus características
    const characters = await fetch('http://localhost:3000/characters').then(characters => characters.json());

    const characters$$ = document.getElementById('characters');
    const title$$ = document.getElementById('title');
    const fighters = [];

    h2$$ = document.createElement('h2');
    h2$$.innerHTML = 'Elige dos personajes para librar una batalla:'

    title$$.appendChild(h2$$);

    let c = 0;

    for (let character of characters) {

        let article$$ = document.createElement('article');
        article$$.classList.add('char');
        let h3$$ = document.createElement('h3');
        h3$$.innerHTML = character.name;
        let img$$ = document.createElement('img');
        img$$.src = character.avatar;
        let h5$$ = document.createElement('h5');
        h5$$.innerHTML = 'Estadísticas';
        let ul$$ = document.createElement('ul');
        for (propiedad in character) { //esto es para pintar las estadísticas 
            if (propiedad === 'critic' || propiedad === 'defense' || propiedad === 'vitality') {
                li$$ = document.createElement('li')
                li$$.innerHTML = propiedad + ': ' + character[propiedad];
                ul$$.appendChild(li$$);
            }
        }

        img$$.addEventListener('click', function () { //esto es para que cuando cliques la imagen, recoja los personajes y se lance la batalla
            c++;
            if (c < 2) {
                article$$.style.border = '5px solid red';
                fighters.push(character);
            } else if (c == 2) {
                article$$.style.border = '5px solid red';
                fighters.push(character);
                setTimeout(function () {
                    btnFight = document.createElement('button');
                    btnFight.innerHTML = "Let's fight!";
                    btnFight.classList.add('btnFight');
                    btnFight.addEventListener('click', function () {
                        printFigther(fighters);
                    })
                    characters$$.appendChild(btnFight);
                }, 300);
            }
        })

        article$$.appendChild(h3$$);
        article$$.appendChild(img$$);
        article$$.appendChild(h5$$);
        article$$.appendChild(ul$$);
        characters$$.appendChild(article$$);
    }
}

const reset = () => {
    const characters$$ = document.getElementById('characters');
    characters$$.innerHTML = ' ';
    const title$$ = document.getElementById('title');
    title$$.innerHTML = ' ';
    const battle$$ = document.getElementById('battle');
    battle$$.innerHTML = ' ';
    const gameover$$ = document.getElementById('gameover');
    gameover$$.innerHTML = ' ';
    round = 0;
    valorTotal1 = 0;
    valorTotal2 = 0;
}

const replay$$ = document.getElementById('replay');
replay$$.addEventListener('click', function () {
    reset();
    printCharacters();
});

printCharacters();